#!/bin/sh

PROJECT_ID=$1
UPSTREAM_BRANCH=$2
ARTIFACT_JOB=$3

URL="https://gitlab.com/api/v4/projects/${PROJECT_ID}/jobs/artifacts/${UPSTREAM_BRANCH}/download?job=${ARTIFACT_JOB}"
echo $URL
#curl --location --output artifacts.zip --header "JOB-TOKEN: $CI_JOB_TOKEN" "$URL"
wget -O artifacts.zip --header="JOB-TOKEN: $CI_JOB_TOKEN" $URL

if [[ $(file --brief artifacts.zip) == "JSON data" ]]; then
  cat artifacts.zip
  exit 1
fi

file artifacts.zip
unzip -d . artifacts.zip
rm artifacts.zip
